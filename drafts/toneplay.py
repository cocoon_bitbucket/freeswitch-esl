from switchio import Client
from switchio import get_listener


from switchio.apps import app
from switchio import event_callback


vm_host = "192.168.99.100"
intermediary_hostname = "192.168.99.100"



@app
class MyTonePlay(object):
    """Play a 'milli-watt' tone on the outbound leg and echo it back
    on the inbound
    """
    @event_callback('CHANNEL_PARK')
    def on_park(self, sess):
        if sess.is_inbound():
            sess.answer()

    @event_callback("CHANNEL_ANSWER")
    def on_answer(self, sess):
        # inbound leg simply echos back the tone
        if sess.is_inbound():
            sess.echo()

        # play infinite tones on calling leg
        if sess.is_outbound():
            sess.playback('tone_stream://%(251,0,1004)',
                          params={'loops': '-1'})



if __name__== "__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)


    client = Client(vm_host)
    r = client.connect()  # could have passed the hostname here as well
    status= client.api('status')  # call ESL `api` command directly


    l = get_listener(vm_host)
    l.connect()
    l.start()
    client.listener = l

    r= client.load_app(MyTonePlay)

    toneplay = client.apps.MyTonePlay['MyTonePlay']
    # < switchio.apps.players.TonePlay at 0x7f7c5fdaf650 >

    r = isinstance(toneplay, MyTonePlay)  # Loading the app type instantiates it
    # True

    job = client.originate('park@%s:5080' % vm_host,
                           proxy='%s:5060' % intermediary_hostname ,
                           app_id= toneplay.cid
                           )
    job.wait(10)  # wait for call to connect

    if job.successful():
        call = client.listener.calls[job.sess_uuid]  # look up the call by originating sess uuid
        call.hangup()
    else:
        raise RuntimeError("failed job: %s" % str(job.result))
