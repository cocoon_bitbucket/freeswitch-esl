from switchio.apps.routers import Router

router = Router(
    guards={
        'Call-Direction': 'inbound',
        'variable_sofia_profile': 'external'},
    #subscribe=('PLAYBACK_START', 'PLAYBACK_STOP'),
)

@router.route('(.*)')
async def welcome(sess, match, router):
    """Say hello to inbound calls.
    """
    await sess.answer()  # resumes once call has been fully answered
    sess.log.info("Answered call to {}".format(match.groups(0)))

    sess.playback(  # non-blocking
        'en/us/callie/ivr/8000/ivr-founder_of_freesource.wav')
    await sess.recv("PLAYBACK_START")
    sess.log.info("Playing welcome message")

    await sess.recv("PLAYBACK_STOP")
    await sess.hangup()  # resumes once call has been fully hungup


if __name__ == '__main__':

    import logging
    logging.basicConfig(level=logging.DEBUG)


    fs_host = "192.168.99.100"


    from switchio import Service

    #service = Service(['fs-host1', 'fs-host2', 'fs-host3'])

    service = Service([fs_host,])

    service.apps.load_app(router, app_id='default')
    service.run()