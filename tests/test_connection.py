from switchio import Client
from switchio import get_listener

from switchio.apps.players import TonePlay

import logging

"""

    PREREQUISITES
    
    * a running freeswitch esl server at 192.168.99.100



"""



vm_host = "192.168.99.100"
intermediary_hostname = "192.168.99.100"


def test_connection():


    client = Client(vm_host)
    r = client.connect()  # could have passed the hostname here as well
    status= client.api('status')  # call ESL `api` command directly
    #<ESL.ESLevent; proxy of <Swig Object of type 'ESLevent *' at 0x28c1d10> >

    local_ip_v4 = client.cmd('global_getvar local_ip_v4')  # `api` wrapper which returns event body content
    assert local_ip_v4 == "10.0.2.15\n"


def test_listener():


    client = Client(vm_host)
    r = client.connect()  # could have passed the hostname here as well
    status= client.api('status')  # call ESL `api` command directly
    #<ESL.ESLevent; proxy of <Swig Object of type 'ESLevent *' at 0x28c1d10> >


    l = get_listener(vm_host)
    l.connect()
    l.start()

    client.listener = l

    client.originate('9196@%s:5080' % vm_host,
                     dp_exten=9197,
                     proxy='%s:5060' % vm_host)

    calls= client.listener.calls

    return

def test_app_toneplay():


    client = Client(vm_host)
    r = client.connect()  # could have passed the hostname here as well
    status= client.api('status')  # call ESL `api` command directly


    l = get_listener(vm_host)
    l.connect()
    l.start()
    client.listener = l

    r= client.load_app(TonePlay)

    toneplay = client.apps.TonePlay['TonePlay']
    # < switchio.apps.players.TonePlay at 0x7f7c5fdaf650 >

    r = isinstance(toneplay, TonePlay)  # Loading the app type instantiates it
    # True

    job = client.originate('park@%s:5080' % vm_host,
                           proxy='%s:5060' % intermediary_hostname ,
                           app_id= toneplay.cid
                           )
    job.wait(10)  # wait for call to connect

    if job.successful():
        call = client.listener.calls[job.sess_uuid]  # look up the call by originating sess uuid
        call.hangup()
    else:
        raise RuntimeError("failed job: %s" % str(job.result))


    return





if __name__=="__main__":


    logging.basicConfig(level=logging.DEBUG)

    #test_connection()
    #test_listener()

    test_app_toneplay()

    print("Done.")