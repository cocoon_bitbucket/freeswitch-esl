from switchio import Client




client = Client('192.168.99.100')
client.connect()  # could have passed the hostname here as well
status= client.api('status')  # call ESL `api` command directly
#<ESL.ESLevent; proxy of <Swig Object of type 'ESLevent *' at 0x28c1d10> >

local_ip_v4 = client.cmd('global_getvar local_ip_v4')  # `api` wrapper which returns event body content
#'10.10.8.21'


print("Done...")