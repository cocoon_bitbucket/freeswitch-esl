#!/usr/bin/env bash


# delete existing conf
rm -rf conf-boot2docker/*

# initialize conf from ci-minimal
cp -r ci-minimal/* conf-boot2docker/

# replace $${local_ip_v4} with boot2docker ip address : 192.168.99.100
ls conf-boot2docker/vars.xml | xargs sed -i '' 's/192.168.99.100/192.168.99.100/g'