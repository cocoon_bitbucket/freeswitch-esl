a minimal freeswitch image for esl switchio

see: https://github.com/friends-of-freeswitch/switchio/tree/master/conf/ci-minimal


conf-boot2doxker is a copy of ci-minimal customized to run in boot2docker ( 192.168.99.100)

replace $${local_ip_v4} with 192.168.99.100
